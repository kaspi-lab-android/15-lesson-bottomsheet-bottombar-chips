package com.example.myapplication

import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import android.widget.CompoundButton
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.activity_bottomsheet.*

class BottomSheetActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val view = View.inflate(this, R.layout.activity_bottomsheet, null)
        setContentView(view)

        val bottomSheetDialog = BottomSheetDialog(this)
        bottomSheetDialog.setContentView(R.layout.dialog_bottomsheet)
        bottomSheetDialog.dismissWithAnimation = true
        val bottomSheetInternal = bottomSheetDialog.findViewById<View>(R.id.design_bottom_sheet)
        BottomSheetBehavior.from(bottomSheetInternal as View).peekHeight = 400

        bottomsheet_button.setOnClickListener {
            bottomSheetDialog.show()
            bottomSheetDialog.setTitle(getText(R.string.cat_bottomsheet_title))
        }

        cat_fullscreen_switch.setOnCheckedChangeListener { buttonView: CompoundButton?, isChecked: Boolean ->
            val params = bottom_drawer.layoutParams
            val bottomSheetBehavior =
                BottomSheetBehavior.from(bottom_drawer)
            bottomSheetBehavior.setUpdateImportantForAccessibilityOnSiblings(isChecked)
            val modalBottomSheetChildView = bottomSheetDialog.findViewById<View>(R.id.bottom_drawer_2)
            val layoutParams = modalBottomSheetChildView!!.layoutParams
            val modalBottomSheetBehavior =
                bottomSheetDialog.behavior
            var fitToContents = true
            var halfExpandedRatio = 0.5f
            val windowHeight = getWindowHeight()
            if (params != null && layoutParams != null) {
                if (isChecked) {
                    params.height = windowHeight
                    layoutParams.height = windowHeight
                    fitToContents = false
                    halfExpandedRatio = 0.7f
                } else {
                    params.height = getBottomSheetPersistentDefaultHeight()
                    layoutParams.height = getBottomSheetDialogDefaultHeight()
                }
                bottom_drawer.layoutParams = params
                modalBottomSheetChildView.layoutParams = layoutParams
                bottomSheetBehavior.isFitToContents = fitToContents
                modalBottomSheetBehavior.isFitToContents = fitToContents
                bottomSheetBehavior.halfExpandedRatio = halfExpandedRatio
                modalBottomSheetBehavior.halfExpandedRatio = halfExpandedRatio
            }
            cat_bottomsheet_expansion_switch.isEnabled = !isChecked
        }

        cat_bottomsheet_expansion_switch.setOnCheckedChangeListener { buttonView: CompoundButton?, isChecked: Boolean ->
            var lp = bottomSheetInternal.layoutParams
            lp.height = if (isChecked) 400 else getBottomSheetDialogDefaultHeight()
            bottomSheetInternal.layoutParams = lp
            lp = bottom_drawer.layoutParams
            lp.height = if (isChecked) 400 else getBottomSheetPersistentDefaultHeight()
            bottom_drawer.layoutParams = lp
            cat_fullscreen_switch.isEnabled = !isChecked
            view.requestLayout()
        }
    }

    private fun getWindowHeight(): Int {
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        return displayMetrics.heightPixels
    }

    private fun getBottomSheetDialogDefaultHeight(): Int {
        return getWindowHeight() * 2 / 3
    }

    private fun getBottomSheetPersistentDefaultHeight(): Int {
        return getWindowHeight() * 3 / 5
    }

}
