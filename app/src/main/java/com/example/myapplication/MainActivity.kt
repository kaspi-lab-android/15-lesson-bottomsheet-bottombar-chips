package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomSheetButton.setOnClickListener {
            startActivity(Intent(this, BottomSheetActivity::class.java))
        }

        bottomBarButton.setOnClickListener {
            startActivity(Intent(this, BottomBarActivity::class.java))
        }

        chipsButton.setOnClickListener {
            startActivity(Intent(this, ChipsActivity::class.java))
        }
    }
}
