package com.example.myapplication

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_chip.*

class ChipsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chip)

        cat_chip_entry_chip.setOnCloseIconClickListener {
            Toast.makeText(this, "Close", Toast.LENGTH_SHORT).show()
        }

        cat_chip_enabled_switch.setOnCheckedChangeListener { _, isChecked ->
            cat_chip_entry_chip.isEnabled = isChecked
            cat_chip_filter_chip.isEnabled = isChecked
            cat_chip_choice_chip.isEnabled = isChecked
            cat_chip_action_chip.isEnabled = isChecked
        }
    }
}
