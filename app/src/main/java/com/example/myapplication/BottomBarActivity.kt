package com.example.myapplication

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.bottomappbar.BottomAppBar
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_bottomappbar.*

class BottomBarActivity : AppCompatActivity() {

    private var bottomDrawerBehavior: BottomSheetBehavior<View>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottomappbar)

        setSupportActionBar(bar)

        setUpBottomDrawer()

        fab.setOnClickListener {
            Toast.makeText(this, "Hello", Toast.LENGTH_SHORT).show()
        }
        val navigationView: NavigationView = findViewById(R.id.navigation_view)
        navigationView.setNavigationItemSelectedListener { item: MenuItem ->
            Snackbar.make(coordinator_layout, item.title, Snackbar.LENGTH_SHORT)
                .setAnchorView((if (fab.visibility == View.VISIBLE) fab else bar) as View)
                .show()
            false
        }

        center.setOnClickListener {
            bar.fabAlignmentMode = BottomAppBar.FAB_ALIGNMENT_MODE_CENTER
        }
        end.setOnClickListener {
            bar.fabAlignmentMode = BottomAppBar.FAB_ALIGNMENT_MODE_END
        }
        hideFab.isChecked = fab.visibility == View.VISIBLE
        hideFab.setOnCheckedChangeListener { _, isChecked: Boolean ->
            if (isChecked) {
                fab.show()
            } else {
                fab.hide()
            }
        }

        hideOnScroll.isChecked = bar.hideOnScroll
        hideOnScroll.setOnCheckedChangeListener { _, isChecked: Boolean ->
            bar.hideOnScroll = isChecked
        }
    }

    override fun onBackPressed() {
        if (bottomDrawerBehavior?.state != BottomSheetBehavior.STATE_HIDDEN) {
            bottomDrawerBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
            return
        }
        return super.onBackPressed()
    }

    private fun setUpBottomDrawer() {
        val bottomDrawer: View = coordinator_layout.findViewById(R.id.bottom_drawer)
        bottomDrawerBehavior = BottomSheetBehavior.from(bottomDrawer)
        bottomDrawerBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
        bar.setNavigationOnClickListener { v: View? ->
            bottomDrawerBehavior?.setState(
                BottomSheetBehavior.STATE_HALF_EXPANDED
            )
        }
        bar.setNavigationIcon(R.drawable.ic_drawer_menu_24px)
        bar.replaceMenu(R.menu.demo_primary)
    }

}
